import 'package:flutter/material.dart';
import 'package:focuse_it/screens/home_screen/home_screen.dart';

void main() => runApp(MaterialApp(
  home: HomeScreen(),
  theme: ThemeData(
    primaryColor: Colors.amber,
    accentColor: Colors.amberAccent,
  ),
));