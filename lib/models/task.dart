class Task{
  int id;
  String title;
  String description;
  bool isDone;
  DateTime deadlineDate;

  Task();

  Map<String, dynamic> toMap(){
    Map<String, dynamic> result = {
      'id' : id,
      'title' : title,
      'description': description,
      'isDone': isDone ? 1 : 0,
      'deadlineDate': deadlineDate.toString(),
    };
    return result;
  }

  factory Task.fromMap(Map<String, dynamic> map){
    Task task = new Task();

    task.id = map['id'];
    task.title = map['title'];
    task.description = map['description'];
    task.isDone = map['isDone'] == 1 ? true : false;
    task.deadlineDate = DateTime.parse(map['deadlineDate']);

    return task;
  }
}