import 'package:flutter/cupertino.dart';
import 'package:focuse_it/models/task.dart';
import 'dart:async';

import 'package:focuse_it/services/database_service.dart';

class HomeScreenChangeNotifier extends ChangeNotifier{
  Timer pomo;

  void createTask(Task task){
    DatabaseService.instance.createTask(task);
  }
  void removeTask(Task task){
    DatabaseService.instance.removeTask(task);
  }
  Future<List<Task>> getTasks() async{
    return await DatabaseService.instance.getTasks();
  }

  void startCountdown(Function callback){
    pomo = new Timer(Duration(minutes: 25), callback);
  }

  void stopCountdown(){
    pomo.cancel();
  }
}