import 'dart:async';
import 'dart:core';

import 'package:focuse_it/models/task.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  static final instance = DatabaseService._();

  DatabaseService._();

  static Database db;

  Future<Database> get database async {
    if (db != null) {
      return db;
    }
    db = await init();
    return db;
  }

  Future<dynamic> init() async {
    String path = await getDatabasesPath() + 'tasks';
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Tasks("
          "id TEXT,"
          "title TEXT,"
          "description TEXT,"
          "isDone INTEGER,"
          "deadlineDate TEXT,"
          ")");
    });
  }

  Future<Task> getTaskById(int id) async {
    Database db = await this.database;
    var data = await db.rawQuery('SELECT * FROM Tasks WHERE id=\'$id\'');
    if (data.isNotEmpty) {
      return Task.fromMap(data[0]);
    } else {
      // Throw exception
    }
  }

  Future<List<Task>> getTasks() async {
    Database db = await this.database;
    var data = await db.rawQuery('SELECT * FROM Tasks');
    if (data.isNotEmpty) {
      List<Task> output = new List<Task>();

      data.forEach((map) {
        output.add(Task.fromMap(map));
      });

      return output;
    } else {
      // Throw exception
    }
  }

  Future<void> createTask(Task task) async {
    Database db = await this.database;
    db.insert('Tasks', task.toMap());
  }

  Future<void> removeTask(Task task) async {
    Database db = await this.database;
    db.delete('Tasks', where: 'id = ?', whereArgs: [task.id]);
  }
}